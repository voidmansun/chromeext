﻿  var links = document.getElementsByTagName('a');

    function fetchLink(link) {
      if (!/^http(s?):\/\//.test(link.href)) return;
      var xhr = new XMLHttpRequest();
      xhr.open("HEAD", link.href, true);
      xhr.onreadystatechange = function() {
        if (xhr.readyState < xhr.HEADERS_RECEIVED || xhr.processed) return;
        if (xhr.status >= 400) {

          link.style.backgroundColor = "red";
          link.title = xhr.status;

        }
        xhr.processed = true;
        xhr.abort();
      }
      try {
        xhr.send(null);
      } catch (e) {
        console.error("XHR failed for " + link.href + ", " + e);
      }
    }

    for (var i = 0; i < links.length; ++i)
    fetchLink(links[i]);
